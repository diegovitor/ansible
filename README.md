# Ambiente para simulacao de cluster Nomad Consul

Este projeto foi organizado para documentar vários modelos de cluster integrando Consul e Nomad. 

O projeto foi dividido em labs para permitir a o treinamento de vários cenários caso haja necessidade. 

O projeto que será utilizado será o lab03 que engloba 3 servidores nomad e consul no mesmo nó e N clients Nomad-Consul

## Projetos com Ansible
A partir do lab04, o códico para configuração de toda infraestrutura do cluster passou a ser escrito em ansible. 

 O ```lab04``` é um projeto para criação de um cluster de 3 servidores e 2 clients. 
 O ```lab05``` é um projeto para criação de um cluster de 1 server e 2 clients.  

1. [Modelo sugerido pela Hashicorp](/lab02/infra/documentacao/modelo_cluster_hashicorp.jpg)
2. Melhorar scripts devido a repetição de código

## Tarefas
* [x] Nomad básico
    * [x] Introdução ao Nomad - [link](https://developer.hashicorp.com/nomad/tutorials/get-started/gs-overview)
    * [x] Instalação Local do Nomad - [link](https://developer.hashicorp.com/nomad/tutorials/get-started/gs-install)
    * [x] Criação de Cluster Local - [link](https://developer.hashicorp.com/nomad/tutorials/get-started/gs-install)
    * [x] Criação de Cluster Local utilizando o Vagrant [link](https://gitlab-sti.ufc.br/diegovitor/cluster-nomad-local)
    * [x] Deploy e atualização de Jobs
    * [x] Teste de deploy do traefik, whoami, metabase e postgres [link](https://gitlab-sti.ufc.br/diegovitor/cluster-nomad-local/-/tree/master/infra/jobs)
    * [x] Teste de comunicação entre services
    * [x] Estudar Redes no Nomad Mais a fundo
    * [ ] Instalar plugin CNI no nomad  
    * [x] Persistência de dados
    * [x] Criação de volumes
* [x] Consul básico
    * [x] Introdução ao Consul - [link]()
    * [x] Instalação do Consul - [link](https://developer.hashicorp.com/consul/downloads)
    * [x] Integração Nomad Consul
* [x] Traefik básico
  * [x] Integração com o Consul
* [ ] Aplicação básica
  * [x] Healthcheck
    - [ ] Realizado estudo apenas de alguns casos
  * [x] Localização da máquina pelo consul no lugar do docker
* [ ] Segurança
  * [x] Senha no Nomad (servidor e cliente)
    - [x] Estudar Tokens ACL
  * [x] Senha no Consul
  * [ ] Senha no Traefik
  * [ ] Migrar para Vault

