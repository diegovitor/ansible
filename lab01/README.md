#  Lab Local usando Nomad e Consul

Ambiente de teste para criação de um cluster de 3 servers utilizando Vagrant.

## Pré-Requisitos

 - Vagrant [https://www.vagrantup.com/] 
 - VirtualBox [https://www.virtualbox.org/]
 - mínimo de 8GB de Ram e processador Core i5 ou superior. 

## Criar o ambiente de simulação

Para criar o cluster local é necessário executar o comando vagrant up no diretório que contém 
o arquivo Vagrantfile. 

```bash 
$ vagrant up
```

após a instalação, o seguinte comando poderá ser executado para verificar o status 
das VM criadas:

```bash 
$ vagrant status
```

Caso tudo tenha ocorrido direito, o terminal mostrará os seguintes hosts criados

```bash
Current machine states:

nomad-server-1            running (virtualbox)
nomad-server-2            running (virtualbox)
nomad-server-3            running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```


o vagrant por padrão cria um usuário vagrant juntament com o par de chaves ssh 

para acessar a VM, basta executar o seguinte comando:


```bash
vagrant ssh $hostname

ex. 
vagrant ssh nomad-server-1
```

## Referências 
  -  HashiCorp Nomad — From Zero to WOW! [link](https://medium.com/hashicorp-engineering/hashicorp-nomad-from-zero-to-wow-1615345aa539)