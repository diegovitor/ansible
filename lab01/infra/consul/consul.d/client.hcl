data_dir = "/opt/consul/client"
log_level = "INFO"
server = false
advertise_addr   = "{{ GetInterfaceIP `eth1` }}"
client_addr = "0.0.0.0"
datacenter = "ufc"

retry_join = ["192.168.56.101", "192.168.56.102", "192.168.56.103"]

# Enable Consul Connect
# https://www.nomadproject.io/docs/integrations/consul-connect
ports {
    grpc = 8502
}

connect {
    enabled = true
}

ui_config {
    enabled = false
}
