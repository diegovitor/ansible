data_dir = "/opt/consul/server"

server           = true
bootstrap_expect = 3
advertise_addr   = "{{ GetInterfaceIP `eth1` }}"
client_addr      = "0.0.0.0"
datacenter       = "ufc"
retry_join       = ["192.168.56.101", "192.168.56.102", "192.168.56.103"]

ui_config {
    enabled = true
}