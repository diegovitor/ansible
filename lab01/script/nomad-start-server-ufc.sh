#!/bin/bash

cd $HOME

# Cluster Consul
ps -C consul
retval=$?
if [ $retval -eq 0 ]; then
  sudo killall consul
fi

echo "Valida configuração do consul"
sudo consul validate /etc/consul.d/

echo "Habilita e inicia Consul"
sudo systemctl enable consul
sudo systemctl start consul
sudo systemctl status consul

#sudo cp /vagrant/consul-server-config/consul-server-ufc.hcl /etc/consul.d/consul-server-ufc.hcl
# sudo nohup consul agent --config-file /etc/consul.d/consul-server-ufc.hcl &>$HOME/consul.log &

# Cluster Nomad 
ps -C nomad
retval=$?
if [ $retval -eq 0 ]; then
  sudo killall nomad
fi
#sudo cp /vagrant/nomad-server-config/nomad-server-ufc.hcl /etc/nomad.d/nomad-server-ufc.hcl
sudo systemctl enable nomad
sudo systemctl start nomad
sudo systemctl status nomad
# sudo nohup nomad agent -config /etc/nomad.d/nomad-client-ufc.hcl &>$HOME/nomad.log &

#seta variáveis de ambiente para que seja possível o acesso ao nomad
IP_ADDRESS = $(ip -o addr show up primary scope global | grep eth1 |while read -r num dev fam addr rest; do echo ${addr%/*}; done)
echo "export NOMAD_ADDR=http://$IP_ADDRESS:4646" | sudo tee --append $HOME/.bashrc

sudo cp -r /vagrant/infra/jobs /home/vagrant

