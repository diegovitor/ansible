CONSUL_SERVER_COUNT = 3
NOMAD_SERVER_COUNT = 3
CLIENT_COUNT = 4
IMAGE = "debian/bullseye64"

Vagrant.configure("2") do |config|

  # disconnect virtuabox serial port
  config.vm.provider "virtualbox" do |vb|
    vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
    vb.memory = "368"
  end

  (1..CONSUL_SERVER_COUNT).each do |i|
    config.vm.define "consul-server0#{i}" do |consulServer|
      consulServer.vm.box = IMAGE
      consulServer.vm.hostname = "consul-server0#{i}"
      consulServer.vm.network "private_network", ip: "192.168.56.#{i+100}", type: "static", bridge: "eth1"
      
      #Para utilizar redes internas e necessario habilitar * 0.0.0.0/0 ::/0 em /etc/vbox/networks.conf
      #consulServer.vm.network "private_network", ip: "10.0.0.#{i+100}", type: "static", bridge: "eth0"

      if i == 1
        # Expose the nomad ports
        consulServer.vm.network "forwarded_port", guest: 8500, host: 8500, auto_correct: true
      end

      consulServer.vm.provision "shell", privileged: true,  path: "scripts/consul_server_install.sh"
      # nomad
    end
  end

  (1..NOMAD_SERVER_COUNT).each do |i|
    config.vm.define "nomad-server0#{i}" do |nomadServer|
      nomadServer.vm.box = IMAGE
      nomadServer.vm.hostname = "nomad-server0#{i}"

      nomadServer.vm.network "private_network", ip: "192.168.56.#{i+120}", type: "static", bridge: "eth1"
      #Para utilizar redes internas e necessario habilitar * 0.0.0.0/0 ::/0 em /etc/vbox/networks.conf
      #nomadServer.vm.network "private_network", ip: "10.0.0.#{i+120}", type: "static", bridge: "eth0"

      nomadServer.vm.network "forwarded_port", guest: 8500, host: 8500, auto_correct: true
      # install client consul
      nomadServer.vm.provision "shell", privileged: true,  path: "scripts/consul_client_install.sh"

      # Expose the nomad ports
      nomadServer.vm.network "forwarded_port", guest: 4646, host: 4646, auto_correct: true
      nomadServer.vm.provision "shell", privileged: true,  path: "scripts/nomad_server_install.sh"
    end
  end

  (CLIENT_COUNT..CLIENT_COUNT).each do |i|
    config.vm.define "client0#{i}" do |consulClients|
      consulClients.vm.box = IMAGE
      consulClients.vm.hostname = "client0#{i}"

      consulClients.vm.network "private_network", ip: "192.168.56.#{i+120}", type: "static", bridge: "eth1"
      #Para utilizar redes internas e necessario habilitar * 0.0.0.0/0 ::/0 em /etc/vbox/networks.conf
      #consulClients.vm.network "private_network", ip: "10.0.0.#{i+120}", type: "static", bridge: "eth0"

      consulClients.vm.network "forwarded_port", guest: 8500, host: 8500, auto_correct: true
      consulClients.vm.provision "shell", privileged: true,  path: "scripts/consul_client_install.sh"

      # Expose the nomad ports
      consulClients.vm.network "forwarded_port", guest: 5656, host: 5656, auto_correct: true
      consulClients.vm.provision "shell", privileged: true,  path: "scripts/nomad_client_install.sh"
    end
  end
end