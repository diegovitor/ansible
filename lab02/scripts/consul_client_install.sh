#!/usr/bin/env bash

#set -e

sudo apt-get update -y
sudo apt-get install unzip vim jq apt-transport-https lsb-release ca-certificates curl gnupg -y

# make an archive folder to move old binaries into
if [ ! -d /tmp/archive ]; then
  sudo mkdir /tmp/archive/
fi

cd /tmp/

echo "Consul Install Beginning..."

# Configura versão do Consul
CONSUL_VERSION=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/consul | jq -r ".current_version")
#CONSUL_VERSION=1.15.1
sudo curl -sSL https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip > consul.zip
if [ ! -d consul ]; then
  sudo unzip /tmp/consul.zip
fi
if [ ! -f /usr/bin/consul ]; then
  sudo install consul /usr/bin/consul
fi
if [ -f /tmp/archive/consul ]; then
  sudo rm /tmp/archive/consul
fi
sudo mv /tmp/consul /tmp/archive/consul
sudo mkdir -p /etc/consul.d
sudo chmod a+w /etc/consul.d

if [[ $(hostname) == "nomad-server01" ]]; then
  sudo cp /vagrant/infra/consul/client01/consul.d/client.hcl /etc/consul.d
fi
if [[ $(hostname) == "nomad-server02" ]]; then
  sudo cp /vagrant/infra/consul/client02/consul.d/client.hcl /etc/consul.d
fi
if [[ $(hostname) == "nomad-server03" ]]; then
  sudo cp /vagrant/infra/consul/client03/consul.d/client.hcl /etc/consul.d
fi
if [[ $(hostname) == "client04" ]]; then
  sudo cp /vagrant/infra/consul/client04/consul.d/client.hcl /etc/consul.d
fi
if [[ $(hostname) == "client05" ]]; then
  sudo cp /vagrant/infra/consul/client05/consul.d/client.hcl /etc/consul.d
fi 
if [[ $(hostname) == "client06" ]]; then
  sudo cp /vagrant/infra/consul/client06/consul.d/client.hcl /etc/consul.d
fi

sudo cp /vagrant/infra/service/client/consul.service /etc/systemd/system

for bin in cfssl cfssl-certinfo cfssljson
do
  echo "$bin Install Beginning..."
  if [ ! -f /tmp/${bin} ]; then
    curl -sSL https://pkg.cfssl.org/R1.2/${bin}_linux-amd64 > /tmp/${bin}
  fi
  if [ ! -f /usr/local/bin/${bin} ]; then
    sudo install /tmp/${bin} /usr/local/bin/${bin}
  fi
done

cd $HOME

# Cluster Consul
ps -C consul
retval=$?
if [ $retval -eq 0 ]; then
  sudo killall consul
fi

# Check that your configuration file is valid, with the Consul CLI validate command.
echo "Valida configuração do consul"
sudo consul validate /etc/consul.d/

echo "Habilita e inicia Consul"
sudo systemctl enable consul
# sudo systemctl start consul
# sudo systemctl status consul
sudo service consul start
sudo service consul status

CONSUL_ADDRES="$(ip -o addr show up primary scope global | grep eth1 |while read -r num dev fam addr rest; do echo ${addr%/*}; done)"
echo "export CONSUL_HTTP_ADDR=http://$CONSUL_ADDRES:8500" | sudo tee --append $HOME/.bashrc