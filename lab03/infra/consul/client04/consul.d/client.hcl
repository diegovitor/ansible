# Consul agent for client04
# 
# Start with:
#   $ consul agent -config-dir=/etc/hashicorp.d/consul.d/


data_dir = "/opt/consul"
log_level = "DEBUG"
enable_syslog = true

node_name = "node-client04"
datacenter = "ufc"

server = false

bind_addr = "0.0.0.0"
client_addr = "192.168.56.204"
advertise_addr = "{{ GetInterfaceIP `eth1` }}"

retry_join       = ["192.168.56.101", "192.168.56.102", "192.168.56.103"]
retry_interval = "15s",
rejoin_after_leave = true

# Enable Consul Connect
# https://www.nomadproject.io/docs/integrations/consul-connect
ports {
    grpc = 8502
}

connect {
    enabled = true
}

ui_config {
    enabled = false
}