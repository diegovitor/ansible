# Consul agent for consul-server01
# 
# Start with:
#   $ consul agent -config-dir=/etc/hashicorp.d/consul.d/

data_dir = "/opt/consul"
log_level = "INFO"
enable_syslog = true

node_name = "node-server01"
datacenter = "ufc"

server = true

bootstrap_expect = 3

bind_addr = "0.0.0.0"
client_addr = "0.0.0.0"
advertise_addr = "{{ GetInterfaceIP `eth1` }}"

retry_join = ["192.168.56.102", "192.168.56.103"]
retry_interval = "15s",
rejoin_after_leave = true

# Enable Consul Connect
# https://www.nomadproject.io/docs/integrations/consul-connect
ports {
    grpc = 8502
}

connect {
    enabled = true
}

# enable UI only on master0
# accessible as http://127.0.0.1:8500/ui/
# use nginx in reverse proxy mode to make it visible outside
# of localhost.
ui_config {
    enabled = true
}
