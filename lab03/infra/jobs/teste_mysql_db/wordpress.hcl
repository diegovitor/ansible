job "wordpress-server" {
  datacenters = ["ufc"]
  type        = "service"

  group "wordpress-server" {
    count = 1
    
    network {
        port "http" { to = 80}
    }


    volume "wordpress" {
      type      = "host"
      read_only = false
      source    = "wordpress-fs"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "wordpress-server" {
      driver = "docker"

      volume_mount {
        volume      = "wordpress"
        destination = "/var/www/html"
        read_only   = false
      }

      env {
        // WORDPRESS_DB_HOST = "mysql-server.service.consul:3306"
        WORDPRESS_DB_USER = "wordpress"
        WORDPRESS_DB_PASSWORD = "wordpress"
        WORDPRESS_DB_NAME = "wpdb"
        WORDPRESS_TABLE_PREFIX = "wp_"
        WORDPRESS_DEBUG  = "1"
      }

      template {
        data = <<EOH
{{- range service "mysql-server" }}
WORDPRESS_DB_HOST="{{ .Address }}:{{ .Port }}"
{{ end -}}
EOH
        destination = "local/env"
        change_mode = "restart"
        env         = true
      }

      config {
        image = "wordpress:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 200
        memory = 256

      }

      service {
          name = "wordpress"
          port = "http"

          // check {
          //     name     = "500 error check"
          //     type     = "http"
          //     protocol = "http"
          //     path     = "/"
          //     interval = "30s"
          //     timeout  = "2s"
          // }
        }

      // service {
      //   name = "wordpress-server"
      //   tags = ["urlprefix-/"]
      //   provider = "consul"
      //   port = "http"

      //   // check {
      //   //   name     = "wordpress running on port 80"
      //   //   type     = "http"
      //   //   protocol = "http"
      //   //   path     = "/"
      //   //   interval = "10s"
      //   //   timeout  = "2s"
      //   // }
      // }
    }
  }
}



