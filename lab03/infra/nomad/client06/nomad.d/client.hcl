data_dir = "/opt/nomad/data"
datacenter = "ufc"

// bind_addr = "192.168.56.124"
bind_addr = "0.0.0.0"

advertise {
  http = "{{ GetInterfaceIP `eth1` }}"
  rpc  = "{{ GetInterfaceIP `eth1` }}"
  serf = "{{ GetInterfaceIP `eth1` }}"
}

client {
    enabled = true

    // Make tasks accessible to outside world. Otherwise they are bound to the NAT interface.
    // "The scheduler chooses from these IP addresses when allocating ports for tasks."
    // cp. https://www.nomadproject.io/docs/configuration/client.html#network_interface
    // network_interface = "eth1"
    servers = ["192.168.56.121"]
}

ports {
    http = 5656
}

consul {
  # Consult agent's HTTP Address
  address = "192.168.56.126:8500"

  # The service name to register the server and client with Consul.
  #ter atencao para nomer as instrucoes corretamente
  client_service_name = "nomad-client06"

  # Auth info for http access
  # auth = user:password

  # Advertise Nomad services to Consul
  # Enables automatically registering the services
  auto_advertise = true

  # Enables the servers and clients bootstrap using Consul
  server_auto_join = true
  client_auto_join = true
}
