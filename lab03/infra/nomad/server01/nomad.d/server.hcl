# ----------- Base Cluster Configuration -----------

# Persist data to a location that will survive a machine reboot.
data_dir = "/opt/nomad/data"
datacenter = "ufc"

# Bind to all addresses so that the Nomad agent is available 
bind_addr = "192.168.56.101"
# Advertise an accessible IP address so the server is reachable by other servers
# and clients. The IPs can be materialized by Terraform or be replaced by an
# init script.
advertise {
  http = "{{ GetInterfaceIP `eth1` }}"
  rpc  = "{{ GetInterfaceIP `eth1` }}"
  serf = "{{ GetInterfaceIP `eth1` }}"
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}
# Network ports can be also set. The default values are:
ports {
  http = 4646
  rpc  = 4647  # only server nodes
  serf = 4648 # only server nodes
}

# Log verbosity (INFO, DEBUG)
log_level = "DEBUG"

# Ship metrics to monitor the health of the cluster and 
# to see task resource usage.
#telemetry {
#    statsite_address = "${var.statsite}"
#    disable_hostname = true
#}

# Enable debug endpoints.
enable_debug = true


server {
  # Enable server mode for the local agent
  enabled = true

  # Number of server nodes to wait for before
  # bootstrapping, depending on cluster size
  bootstrap_expect = 3
  server_join {
    retry_join = ["192.168.56.102","192.168.56.103"]
  }
}


# Consul Configuration
consul {
  # Consult agent's HTTP Address
  address = "192.168.56.101:8500"

  # The service name to register the server and client with Consul.
  server_service_name = "node-server-01"
  // client_service_name = "nomad-client01"

  # Auth info for http access
  #auth = user:password

  # Advertise Nomad services to Consul
  # Enables automatically registering the services
  auto_advertise = true

  # Enables the servers and clients bootstrap using Consul
  server_auto_join = true
  client_auto_join = true
}



