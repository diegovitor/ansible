#!/usr/bin/env bash

#set -e

echo "Nomad Install Beginning..."

# Configura versão do Nomad
NOMAD_VERSION=1.5.3
cd /tmp/
sudo curl -sSL https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -o nomad.zip
if [ ! -d nomad ]; then
  sudo unzip nomad.zip
fi
if [ ! -f /usr/bin/nomad ]; then
  sudo install nomad /usr/bin/nomad
fi
if [ -f /tmp/archive/nomad ]; then
  sudo rm /tmp/archive/nomad
fi
sudo mv /tmp/nomad /tmp/archive/nomad
sudo mkdir -p /etc/nomad.d
sudo chmod a+w /etc/nomad.d

if [[ $(hostname) == "node-server01" ]]; then
  sudo cp /vagrant/infra/nomad/server01/nomad.d/server.hcl /etc/nomad.d
fi
if [[ $(hostname) == "node-server02" ]]; then
  sudo cp /vagrant/infra/nomad/server02/nomad.d/server.hcl /etc/nomad.d
fi 
if [[ $(hostname) == "node-server03" ]]; then
  sudo cp /vagrant/infra/nomad/server03/nomad.d/server.hcl /etc/nomad.d
fi

sudo cp /vagrant/infra/service/server/nomad.service /etc/systemd/system

cat /root/.bashrc | grep  "complete -C /usr/bin/nomad nomad"
retval=$?
if [ $retval -eq 1 ]; then
  nomad -autocomplete-install
fi

# Cluster Nomad 
ps -C nomad
retval=$?
if [ $retval -eq 0 ]; then
  sudo killall nomad
fi
#sudo cp /vagrant/nomad-server-config/nomad-server-ufc.hcl /etc/nomad.d/nomad-server-ufc.hcl
sudo systemctl enable nomad
# sudo systemctl start nomad
# sudo systemctl status nomad
sudo service nomad start
sudo service nomad status
# sudo nohup nomad agent -config /etc/nomad.d/nomad-client-ufc.hcl &>$HOME/nomad.log &

#seta variáveis de ambiente para que seja possível o acesso ao nomad
NOMAD_ADDRES="$(ip -o addr show up primary scope global | grep eth1 |while read -r num dev fam addr rest; do echo ${addr%/*}; done)"
echo "export NOMAD_ADDR=http://$NOMAD_ADDRES:4646" | sudo tee --append $HOME/.bashrc

# echo "export NOMAD_ADDR=http://192.168.56.121:4646" | sudo tee --append $HOME/.bashrc
# export CONSUL_HTTP_ADDR=http://192.168.56.101:8500