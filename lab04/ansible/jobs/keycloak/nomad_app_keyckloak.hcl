variables {
  KC_PROXY_ADDRESS_FORWARDING = "true"
  KC_HOSTNAME_STRICT          = "false"
  //   KC_HOSTNAME                 = "auth.localhost"
  KC_PROXY                = "edge"
  KC_HTTP_ENABLED         = "true"
  KC_DB                   = "postgres"
  KC_DB_USERNAME          = "postgres"
  KC_DB_PASSWORD          = "postgres"
  KEYCLOAK_ADMIN          = "admin"
  KEYCLOAK_ADMIN_PASSWORD = "admin"
  KC_HTTP_RELATIVE_PATH   = "/auth" 
}

job "keyckloak" {
  datacenters = ["ufc"]
  type        = "service"

  group "keycloak" {
    network {
      port "admin" {
        to = 8081
      }
    }

    restart {
      interval = "30m"
      attempts = 2
      delay    = "15s"
      mode     = "fail"
    }

    service {
      name     = "keycloak"
      port     = "admin"
      provider = "consul"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.keycloak.entrypoints:web,websecure",
        "traefik.http.routers.keycloak.rule=Host(`keycloak.localhost`)",
        // "traefik.consulcatalog.connect=true",
        // "traefik.http.routers.keycloak.tls=true",
      ]

      check {
        name     = "health"
        type     = "http"
        port     = "admin"
        path     = "/health"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "keycloak" {

      driver = "docker"

      config {
        image        = "https://quay.io/keycloak/keycloak:latest"
        // args         = ["start", "--http-port=8081"]
        args         = ["start"]
        // network_mode = "host"
        ports        = ["admin"]
      }

      env {
        KC_PROXY_ADDRESS_FORWARDING = "${var.KC_PROXY_ADDRESS_FORWARDING}"
        KC_HOSTNAME_STRICT          = "${var.KC_HOSTNAME_STRICT}"
        KC_PROXY        = "${var.KC_PROXY}"
        KC_HEALTH_ENABLED= "true"
        KC_HTTP_ENABLED = "${var.KC_HTTP_ENABLED}"
        KC_DB           = "${var.KC_DB}"
        KC_HTTP_PORT = "${NOMAD_PORT_admin}"
        KC_DB_USERNAME          = "${var.KC_DB_USERNAME}"
        KC_DB_PASSWORD          = "${var.KC_DB_PASSWORD}"
        KEYCLOAK_ADMIN          = "${var.KEYCLOAK_ADMIN}"
        KEYCLOAK_ADMIN_PASSWORD = "${var.KEYCLOAK_ADMIN_PASSWORD}"
        // KC_HTTP_RELATIVE_PATH	= "${var.KC_HTTP_RELATIVE_PATH}"
      }
      template {
        data = <<EOH
{{- range service "db-keycloak" }}
  KC_DB_URL = "jdbc:postgresql://{{ .Address }}:{{ .Port }}/postgres?ssl=allow"
{{ end -}}
EOH

        destination = "local/file.env"
        change_mode = "restart"
        env         = true
      }

      resources {
        cpu    = 1000
        memory = 1024
      }
    }
  }
}

