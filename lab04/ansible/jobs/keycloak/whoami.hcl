job "whoami" {
  datacenters = ["ufc"]
  type = "service"

  group "whoami" {
    count = 1

    network {
      port "web" {
        to = 80
      }
    }

    service {
      name = "whoami"
      provider= "consul"
      port = "web"

      tags = [
        "traefik",
        "traefik.enable=true",
        "traefik.http.routers.http.rule=Host(`whoami.nomad.localhost`)",
        // "traefik.consulcatalog.connect=true",
        // "traefik.http.routers.whoami.rule=Host(`whoami1.localhost`)",
        // "traefik.http.routers.whoami.rule=Path(`/whoami`)",
        // "traefik.http.routers.whoami.entrypoints=web,websecure",
      ]

      check {
        type     = "http"
        path     = "/health"
        port     = "web"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "whoami" {
      driver = "docker"

      config {
        image = "traefik/whoami"
        ports = ["web"]

        // args  = ["--port", "${NOMAD_PORT_web}"]
      }

      env {
        WHOAMI_PORT_NUMBER = "${NOMAD_PORT_web}"
      }

      resources {
        cpu    = 100
        memory = 128
      }
    }
  }
}
