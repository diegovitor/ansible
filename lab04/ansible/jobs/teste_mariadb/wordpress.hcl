job "wordpress-server" {
  datacenters = ["ufc"]
  type        = "service"

  group "server" {
    network {

      port "http" {
        static = 8080
        to     = 80
      }
    }

    service {
      name = "wordpress"
      port = "8080"
      tags = ["production", "wordpress"]
    }


    task "server" {
      driver = "docker"

      config {
        image = "wordpress"
        ports = ["http"]
        network_mode = "host"
      }

      env {
        WORDPRESS_DB_USER     = "exampleuser"
        WORDPRESS_DB_PASSWORD = "examplepass"
        WORDPRESS_DB_NAME     = "exampledb"
      }

      template {
        data = <<EOH
{{- range service "database" }}
WORDPRESS_DB_HOST="{{ .Address }}:{{ .Port }}"
{{ end -}}
EOH
        destination = "local/env"
        change_mode = "restart"
        env         = true
      }

      resources {
        cpu    = 100
        memory = 200
      }
    }
  }

}



