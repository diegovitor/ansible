// https://learn.hashicorp.com/tutorials/nomad/access-control-policies
namespace "default" {
  capabilities = [
    # Necessário permissão de lista para conseguir parar um ambiente
    "list-jobs",
    "read-job",
    "submit-job",
  ]
}
