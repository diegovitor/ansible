job "egw" {

  datacenters = ["ufc"]

  group "egw" {
    count = 2
    network {
      mode = "bridge"
    }

    service {
      name = "egw"

      connect {
        gateway {
          proxy {}
          terminating {
            service {
              name = "postgres"
            }
            service {
              name = "redis-svc"
            }
          }
        }
      }
    }
  }
}
