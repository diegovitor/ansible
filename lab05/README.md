#  Lab Local usando Nomad e Consul

Ambiente de teste para criação de um cluster de 1 server e 2 clients utilizando Vagrant.

## Pré-Requisitos

 - Vagrant [https://www.vagrantup.com/] 
 - VirtualBox [https://www.virtualbox.org/]
 - mínimo de 8GB de Ram e processador Core i5 ou superior. 

## Abaixo segue o modelo do cluster que sera implementado
Neste modelo sao utilizados 3 nos para montar o cluster, sendo 1 servidor Consul-Nomad e  2 clientes Consul-Nomad. 

![modelo](/infra/documentacao/modelo_cluster_hashicorp.jpg)

## Criar o ambiente de simulação

Para criar o cluster local é necessário executar o comando vagrant up no diretório que contém 
o arquivo Vagrantfile. 

```bash 
$ vagrant up
```

após a instalação, o seguinte comando poderá ser executado para verificar o status 
das VM criadas:

```bash 
$ vagrant status
```

Caso tudo tenha ocorrido direito, o terminal mostrará os seguintes hosts criados

```bash
Current machine states:

vm01            running (virtualbox)
vm02            running (virtualbox)
vm03            running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```


o vagrant por padrão cria um usuário vagrant juntament com o par de chaves ssh 

para acessar a VM, basta executar o seguinte comando:


```bash
vagrant ssh $hostname

ex. 
vagrant ssh nomad-server-1
```

