job "example" {
  datacenters = ["ufc"]

  group "cache" {
    task "redis" {
      driver = "docker"

      config {
        image = "redis:3.2"
      }
    }

    task "cron" {
      driver = "raw_exec"

      config {
        command = "/bin/bash"
        args    = ["local/script.sh"]
      }

      template {
        data        = <<EOF
while true; do
  nomad alloc exec -task redis $NOMAD_ALLOC_ID redis-cli ping
  sleep 3
done
EOF
        destination = "local/script.sh"
      }
    }
  }
}

# ref. https://discuss.hashicorp.com/t/nomad-cron-periodic-jobs-in-docekrized-huge-legacy-app/20119/4 