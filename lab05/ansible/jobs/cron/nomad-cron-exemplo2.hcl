job "cron-example" {
  datacenters = ["ufc"]
  type = "batch"

  periodic {
    cron             = "*/1 * * * * *"
    prohibit_overlap = true
  }

  group "cron-teste" {
    task "cron" {
      driver = "raw_exec"

      config {
        command = "/bin/bash"
        args    = ["local/script.sh"]
      }

      template {
        data        = <<EOF
echo "Teste - `date`"
EOF
        destination = "local/script.sh"
      }
    }
  }
}

# ref. https://discuss.hashicorp.com/t/nomad-cron-periodic-jobs-in-docekrized-huge-legacy-app/20119/4 