job "cron-example" {
  datacenters = ["ufc"]
  type = "batch"

  periodic {
    cron             = "*/1 * * * * *"
    prohibit_overlap = true
  }

  group "cron-teste" {
    task "cron" {
      driver = "raw_exec"

      config {
        command = "/bin/bash"
        args    = ["local/script.sh"]
      }

      // env {
      //   PGPASSWORD="postgres"
      // }

      template {
        data        = <<EOF
{{- range service "db-keycloak" }}
nomad alloc exec -token 11ac9fe2-59a3-c098-be06-a8689b90042e -address http://192.168.56.101:4646 -task db e6d1842f-8ea6-e51e-07af-69a12b9f758d /bin/bash -c "PGPASSWORD=postgres pg_dump --inserts --column-inserts --username=postgres --host={{ .Address }} --port={{ .Port }} postgres" > /opt/nomad/dbbackup.sql
{{ end -}}
EOF
        destination = "local/script.sh"
      }
    }
  }
}

# ref. https://discuss.hashicorp.com/t/nomad-cron-periodic-jobs-in-docekrized-huge-legacy-app/20119/4 