job "cron-example" {
  datacenters = ["ufc"]
  type = "batch"

  periodic {
    cron             = "*/1 * * * * *"
    prohibit_overlap = true
  }

  group "cron-teste" {
    task "cron" {
      driver = "raw_exec"

      config {
        command = "/bin/bash"
        args    = ["local/script.sh"]
      }

      env {
        PGPASSWORD="postgres"
      }

      template {
        data        = <<EOF
set -e

echo $DBHOST
echo $DBPORT
echo $DB_ALLOC_ID
echo $PGPASSWORD
EOF
        destination = "local/script.sh"
      }

      template {
        data        = <<EOH
# as service 'db-keycloak' is registered in Consul
# we wat to grab its 'alloc' tag
{{$allocID := env "NOMAD_ALLOC_ID" -}}

{{ range service "db-keycloak" }}
{{ .Tags }}
##
NOMAD_ALLOC_ID = {{env "NOMAD_ALLOC_ID"}}
##
{{ "foo=bar" | split "=" }}
##
DB_ALLOC_ID {{ index .ServiceMeta "allocID" }}
{{ end }}
EOH
        destination = "secrets/file.env"
      }
    }
  }
}

# ref. https://discuss.hashicorp.com/t/nomad-cron-periodic-jobs-in-docekrized-huge-legacy-app/20119/4 
// {{- range service "db-keycloak" }}
// {{ end -}}


// nomad alloc exec \
// -token 67e80eee-c6fa-31a2-366f-724b5b80edd2 \
// -address http://192.168.56.101:4646 \
// -task db $DB_ALLOC_ID \ 
// /bin/bash -c "PGPASSWORD=postgres pg_dump --inserts --column-inserts --username=postgres --host=$DBHOST --port=$DBPORT postgres"  \
// > /opt/nomad/dbbackup-$(date "+%Y-%m-%d---%H-%M-%S").sql