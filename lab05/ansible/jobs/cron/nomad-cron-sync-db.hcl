job "postgres-script-sync" {
  type = "batch"
  datacenters = ["ufc"]


  constraint {
    attribute = "${attr.unique.name}"
    operator = "!="
    value = "vm01"
      
  }

  periodic {
    // Launch every hour
    cron = "*/5 * * * *"

    // Do not allow overlapping runs.
    prohibit_overlap = true
  }

  task "run-mysql-backup" {
    driver = "raw_exec"

    config {
      # When running a binary that exists on the host, the path must be absolute
      command = ""
    }
  }
}