variable "datacenters" {
  description = "Lista de Data Centers"
  type        = list(string)
  default     = ["ufc"]
}

variable "region" {
  description = "Região"
  type        = string
  default     = "global"
}

variable "postgres_db" {
  description = "Nome do Banco"
  default     = "postgres"
}

variable "postgres_user" {
  description = "Usuário Postgres"
  type        = string
  default     = "postgres"
}

variable "postgres_password" {
  description = "Postgres DB Password"
  type        = string
  default     = "postgres"
}

job "postgres" {
  datacenters = var.datacenters
  region = var.region
  type = "service"

  # realizar o deploy do job na vm01
  constraint {
    attribute = "${node.unique.name}"
    operator  = "="
    value     = "vm02"
  }

 group "db-keycloak" {

    count = 1

    network {
      port "db" {
        to = 5432
        static = 15432
      }
    }

    service {
      name = "db-keycloak"
      provider = "consul"
      port = "db"

      # backup service will rely on that particular 'alloc' tag
      tags = ["alloc=${NOMAD_ALLOC_ID}"]

      meta {
        job = "${NOMAD_JOB_ID}"
        group = "${NOMAD_GROUP_NAME}"
        allocID = "${NOMAD_ALLOC_ID}"
      }
    }

    task "db" {
      driver = "docker"

      meta {
        service = "postgres"
      }

      config {
        image   = "postgres"
        ports = ["db"]
      }
      
      env {
        POSTGRES_DB       = var.postgres_db
        POSTGRES_USER     = var.postgres_user
        POSTGRES_PASSWORD = var.postgres_password
      }
    }
  }
}
