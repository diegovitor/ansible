job "security" {
  type = "service"
  datacenters = ["nomadder1"]

    reschedule {
      delay          = "10s"
      delay_function = "constant"
      unlimited      = true
    }

  group "keycloak-db" {
     restart {
       attempts = -1
       interval = "5s"
       delay = "5s"
       mode = "delay"
     }
    volume "security_postgres_volume" {
      type      = "host"
      source    = "security_postgres_volume"
      read_only = false
    }

    count = 1
    network {
      mode = "bridge"
      port "db" {
        to = 5432
      }
    }

    service {
      name = "security-postgres"
      port = "5432"
      connect {
        sidecar_service {}
      }
     check {
        name     = "security_postgres_ping"
        type     = "script"
        command  = "pg_isready"
        task     = "security_postgres"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "security_postgres" {
      volume_mount {
        volume      = "security_postgres_volume"
        destination = "/var/lib/postgresql/data/pgdata"
      }
      driver = "docker"
      env {
        POSTGRES_USER        = "keycloak"
        POSTGRES_DB          = "keycloak"
        PGDATA               = "/var/lib/postgresql/data/pgdata"
        POSTGRES_INITDB_ARGS = "--encoding=UTF8"
      }
      config {
        image = "registry.cloud.private/postgres:14.5"
        volumes = [
          "local/initddb.sql:/docker-entrypoint-initdb.d/initddb.sql"
        ]
        ports = ["db"]
      }
      resources {
        cpu    = 1000
        memory = 2048
      }
      template {
        data = <<EOF
           CREATE SCHEMA IF NOT EXISTS keycloak;
         EOF
        destination = "local/initddb.sql"
      }
      template {
              destination = "${NOMAD_SECRETS_DIR}/env.vars"
              env         = true
              change_mode = "restart"
              data        = <<EOF
      {{- with nomadVar "nomad/jobs/security" -}}
        POSTGRES_PASSWORD    = {{.keycloak_db_password}}
      {{- end -}}
      EOF
           }
    }
  }

  group "keycloak-ingress" {
     restart {
       attempts = -1
       interval = "5s"
       delay = "5s"
       mode = "delay"
     }
    volume "ca_cert" {
      type      = "host"
      source    = "ca_cert"
      read_only = true
    }
    count = 1
    network {
      mode = "bridge"
      port "auth" {
        to = 4181
      }
    }
    service {
      name = "forwardauth"
      port = "auth"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.forwardauth.entrypoints=https",
        "traefik.http.routers.forwardauth.rule= Path(`/_oauth`)",
        "traefik.http.routers.forwardauth.middlewares=traefik-forward-auth",
        "traefik.http.routers.traefik-forward-auth.tls=true",
        "traefik.http.middlewares.traefik-forward-auth.forwardauth.address=http://forwardauth.service.consul:${NOMAD_HOST_PORT_auth}",
        "traefik.http.middlewares.traefik-forward-auth.forwardauth.authResponseHeaders= X-Forwarded-User",
        "traefik.http.middlewares.traefik-forward-auth.forwardauth.authResponseHeadersRegex= ^X-",
        "traefik.http.middlewares.traefik-forward-auth.forwardauth.trustForwardHeader=true",
      #  "traefik.http.middlewares.test-auth.forwardauth.tls.insecureSkipVerify=true"
      ]


    }
      task "await-for-keycloak" {
        driver = "docker"

        config {
          image        = "registry.cloud.private/busybox:1.28"
          command      = "sh"
          args         = ["-c", "echo -n 'Waiting for service keycloak'; until nslookup keycloak.service.consul 2>&1 >/dev/null; do echo '.'; sleep 2; done"]
          #network_mode = "host"
        }

        resources {
          cpu    = 200
          memory = 128
        }

        lifecycle {
          hook    = "prestart"
          sidecar = false
        }
      }
    task "forwardauth" {
      driver = "docker"
      env {
        #        https://brianturchyn.net/traefik-forwardauth-support-with-keycloak/
        #        https://github.com/mesosphere/traefik-forward-auth/issues/36
        #        INSECURE_COOKIE = "1"
        ENCRYPTION_KEY = "45659373957778734945638459467936" #32 character encryption key
        #        SCOPE = "profile email openid" # scope openid is necessary for keycloak...
        SECRET        = "9e7d7b0776f032e3a1996272c2fe22d2"
        PROVIDER_URI  = "https://security.cloud.private/realms/nomadder"
        #        OIDC_ISSUER   = "https://security.cloud.private/realms/nomadder"
        CLIENT_ID     = "ingress"
        LOG_LEVEL     = "debug"
        # Lifetime of cookie 60s
        LIFETIME = "60"

      }
      volume_mount {
        volume      = "ca_cert"
        destination = "/etc/ssl/certs/"
      }
      config {
        image = "registry.cloud.private/mesosphere/traefik-forward-auth:3.1.0"
        ports = ["auth"]
      }
      resources {
        cpu    = 500
        memory = 256
      }
      template {
              destination = "${NOMAD_SECRETS_DIR}/env.vars"
              env         = true
              change_mode = "restart"
              data        = <<EOF
      {{- with nomadVar "nomad/jobs/security" -}}
        CLIENT_SECRET      = {{.keycloak_ingress_secret}}
      {{- end -}}
      EOF
           }
      }
  }

  group "keycloak" {
     restart {
       attempts = -1
       interval = "5s"
       delay = "5s"
       mode = "delay"
     }
    count = 1
    network {
      mode = "bridge"
      port "ui" {
        to = 8080
      }
    }

    service {
      name = "keycloak"
#      port = "ui"
      port = "8080"
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "security-postgres"
              local_bind_port  = 5432
            }
          }
        }
      }
      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.keycloak.tls=true",
        "traefik.http.routers.keycloak.rule=Host(`security.cloud.private`)",
      ]


      check {
        name  = "health"
        type  = "http"
        port ="ui"
        path="/health"
        interval = "10s"
        timeout  = "2s"
      }
    }
      task "await-for-security-postgres" {
        driver = "docker"

        config {
          image        = "registry.cloud.private/busybox:1.28"
          command      = "sh"
          args         = ["-c", "echo -n 'Waiting for service security-postgres'; until nslookup security-postgres.service.consul 2>&1 >/dev/null; do echo '.'; sleep 2; done"]
          #network_mode = "host"
        }

        resources {
          cpu    = 200
          memory = 128
        }

        lifecycle {
          hook    = "prestart"
          sidecar = false
        }
      }
    task "keycloak" {
      driver = "docker"
      env {
        KEYCLOAK_ADMIN  = "admin"
        KC_HTTP_ENABLED= "true"
        KC_HOSTNAME_STRICT_HTTPS="false"
        KC_HEALTH_ENABLED= "true"
        KC_HOSTNAME="security.cloud.private"
        KC_PROXY="edge"
        KC_DB                     = "postgres"
        KC_DB_SCHEMA              = "keycloak"
        KC_DB_USERNAME            = "keycloak"
        KC_DB_URL_HOST            = "${NOMAD_UPSTREAM_IP_security_postgres}"
        KC_DB_URL_PORT            = "${NOMAD_UPSTREAM_PORT_security_postgres}"
      }
      config {
        image = "registry.cloud.private/stack/core/keycloak:20.0.2.0"
        ports = ["ui"]
        args = [
          "start", "--import-realm" , "--optimized"
        ]
      }
      resources {
        cpu    = 1000
        memory = 2048
      }
    template {
            destination = "${NOMAD_SECRETS_DIR}/env.vars"
            env         = true
            change_mode = "restart"
            data        = <<EOF
    {{- with nomadVar "nomad/jobs/security" -}}
      KEYCLOAK_ADMIN_PASSWORD      = {{.keycloak_password}}
      KC_DB_PASSWORD               = {{.keycloak_db_password}}
      KC_NOMADDER_CLIENT_SECRET    = {{.keycloak_ingress_secret}}
      KC_NOMADDER_CLIENT_SECRET_GRAFANA    = {{.keycloak_secret_observability_grafana}}
    {{- end -}}
    EOF
         }
    }
  }
}