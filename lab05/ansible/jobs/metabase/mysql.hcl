job "mysql-server" {
  datacenters = ["ufc"]
  type        = "service"

  group "mysql-server" {
    count = 1

    network {
      port "db" {
        static = 3306
      }
    }

    volume "mysql" {
      type      = "host"
      read_only = false
      source    = "mysql"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "mysql-server" {
      driver = "docker"

      volume_mount {
        volume      = "mysql"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      env = {
        MYSQL_ROOT_PASSWORD = "somewordpress"
        MYSQL_DATABASE = "wpdb"
        MYSQL_USER = "wordpress"
        MYSQL_PASSWORD = "wordpress"
      }

      config {
        image = "mysql"
        ports = ["db"]
      }

      resources {
        cpu    = 200
        memory = 512

      }

      service {
        name = "mysql-server"
        provider = "consul"
        port = "db"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
