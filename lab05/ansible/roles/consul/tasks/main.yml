---
# Instalação e configuração do Consul

# - Cria grupo consul
- name: Create Consul group
  group: name={{ consul_group }} state=present system=yes

# - Cria usuário consul vinculado ao grupo consul
- name: Create consul user
  user: name={{ consul_user }} group={{ consul_group }} createhome=no system=yes

# - Cria diretórios do consul
- name: Create consul directory
  file: >
    state=directory
    path={{ item }}
    owner={{ consul_user }}
    group={{ consul_group }}
    mode=0755
  loop:
    - "{{ consul_install_dir }}"
    - "{{ consul_data_dir }}"
    - "{{ consul_config_dir }}"

- name: Obtem a ultima versão do consul e armazena na variável consul_version
  shell:
    cmd: curl -s https://checkpoint-api.hashicorp.com/v1/check/consul | jq -r ".current_version"
  register: version
- name: Variável criada para armazenar a versão mais recendo do consul obtido na linha acima 
  set_fact:
    consul_version: "{{ version.stdout }}"

# - baixa o consul no diretório consul_download_dir
- name: Download do Consul versão {{ consul_version }}
  get_url: 
    url={{ consul_url }}
    dest={{ consul_download_dir }}/consul.zip

- name: Descompacta o Consul no diretório de instalação
  command: unzip {{ consul_download_dir }}/consul.zip -d {{ consul_install_dir }}
  args:
    creates: "{{ consul_install_dir }}/consul"
  tags: consul

- name: Check for consul log directory
  stat: >
    path={{ consul_log_file|dirname }}
  register: logdir

- name: Create log directory if it does not exist
  file: >
    state=directory
    path={{ consul_log_file|dirname }}
    owner={{ consul_user }}
    group={{ consul_group }}
  when: not logdir.stat.exists

- name: Touch the log file
  file: >
    state=touch
    path={{ consul_log_file }}
    owner={{ consul_user }}
    group={{ consul_group }}
  changed_when: false

- import_tasks: consul_keygen.yml
  
- name: Copia arquivo de configuração consul
  template: 
    src=consul.hcl.j2
    dest={{ consul_config_dir }}/consul.hcl
    owner={{ consul_user }}
    group={{ consul_group }}
    mode=0755

# See https://github.com/ansible/ansible/issues/13652
- name: Se arquivo de serviço já existe, então o remova
  file: 
    state=absent
    path={{ consul_service_dir }}/consul.service

- name: Cria arquivo de serviço 
  template: 
    src=consul.service.j2
    dest={{ consul_service_dir }}/consul.service
    owner=root
    group=root
    mode=0755
  notify: 
    - reload systemd

- name: Força a execução do handlers para que os serviços sejam reiniciados
  meta: flush_handlers

- name: Ensure Consul service is started and enabled on boot
  service: name={{ consul_service_name }} state=started enabled=yes

- import_tasks: consul_acl_token.yml
  when: consul_acl_enable|bool == true
 

- name: Insere as variáveis de ambiente defaul CONSUL_HTTP_ADDR e CONSUL_HTTP_TOKEN
  shell: "{{ item }}"
  loop:
    - "echo \"export CONSUL_HTTP_ADDR=http://{{ consul_address }}:8500\" | sudo tee --append $HOME/.bashrc"
    - "echo \"export CONSUL_HTTP_TOKEN={{ consul_acl_token }}\" | sudo tee --append $HOME/.bashrc" 
  run_once: true
