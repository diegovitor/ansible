#  Lab Local usando Nomad e Consul

Ambiente de teste para criação de um cluster de 3 servers utilizando Vagrant.

## Pré-Requisitos

 - Vagrant [https://www.vagrantup.com/] 
 - VirtualBox [https://www.virtualbox.org/]
 - mínimo de 8GB de Ram e processador Core i5 ou superior. 


## Estrutura do Cluster 
A princípio, o cluster terá 3 nós com o Nomad e o Consul em modo server e 2 nós com o Nomad e o Consul em modo client conforme figura abaixo. A figura mostra um modelo com 3 clients, mas poderia ser um número `N`. Para economizar recurso, optei por 2 clients.

![Modelo de cluster](/doc/img/Modelo%20Resumido%20Hashicorp.jpg)

### 3 server nodes:
- 192.168.56.101 (Lider)
- 192.168.56.102
- 192.168.56.103

### 2 client nodes:
- 192.168.56.204
- 192.168.56.205

## Ambiente de simulação
Para criar o cluster local, é necessário executar o comando vagrant up no diretório que contém o arquivo Vagrantfile. 

```bash 
$ vagrant up
```

após a instalação, o seguinte comando poderá ser executado para verificar o status 
das VM criadas:

```bash 
$ vagrant status
```

Caso tudo tenha ocorrido direito, o terminal mostrará os seguintes hosts criados

```bash
Current machine states:

vm01                      running (virtualbox)
vm02                      running (virtualbox)
vm03                      running (virtualbox)
vm04                      running (virtualbox)
vm05                      running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

o vagrant por padrão cria um usuário vagrant juntament com o par de chaves ssh 

para acessar a VM, basta executar o seguinte comando:

```bash
vagrant ssh $hostname

ex. 
vagrant ssh vm01
```

