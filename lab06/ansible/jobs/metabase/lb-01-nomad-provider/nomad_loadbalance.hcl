job "traefik" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "load-balancer" {
    count = 1
    network {
      port "http" {
        static = 80
      }

      port "https" {
        static = 443
      }

      port "dashboard" {
        static = 8080
      }
    }

    service {
      name = "lb"
      port = "http"
      provider= "nomad"

      tags = [
            // "traefik",
            "traefik.enable=true",
            "traefik.http.routers.lb.rule=Host(`traefik.localhost`)",
            "traefik.http.routers.lb.service=api@internal",
            "traefik.http.routers.lb.tls=false",
            "traefik.http.routers.lb.entrypoints=web,websecure,traefik",
      ]

      check {
        type     = "http"
        port     = "dashboard"
        path     = "/ping"
        interval = "10s"
        timeout  = "2s"
      }


      check {
        name     = "ready-tcp-http"
        type     = "tcp"
        port     = "http"
        interval = "3s"
        timeout  = "2s"
      }

      // check {
      //   name     = "ready-http"
      //   type     = "http"
      //   port     = "http"
      //   path     = "/"
      //   interval = "3s"
      //   timeout  = "2s"
      // }  

      check {
        name     = "ready-tcp-dashboard"
        type     = "tcp"
        port     = "dashboard"
        interval = "3s"
        timeout  = "2s"
      }

      check {
        name     = "ready-http-dashboard"
        type     = "http"
        port     = "dashboard"
        path     = "/"
        interval = "3s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"

      config {
        image        = "traefik:2.10"
        network_mode = "host"
        ports        = ["http", "https","dashboard"]

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
        ]
      }

      template {
        data = <<EOF
[entryPoints]
  [entryPoints.web]
    address = ":{{ env "NOMAD_ALLOC_PORT_http" }}"
  [entryPoints.traefik]
    address = ":{{ env "NOMAD_ALLOC_PORT_dashboard" }}"
  [entryPoints.websecure]
    address = ":{{ env "NOMAD_ALLOC_PORT_https" }}"

[ping]
  entryPoint = "traefik"

[api]
  dashboard = true
  insecure = true
  debug = true

[providers.nomad]
  exposedByDefault = false

  [providers.nomad.endpoint]
    address = "{{ env "NOMAD_ADDR" }}"
EOF

        destination = "local/traefik.toml"
      }

      resources {
        cpu    = 300
        memory = 50
      }

    }
  }
}