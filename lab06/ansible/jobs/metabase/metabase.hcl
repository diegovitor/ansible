variable "datacenters" {
  description = "Lista de Data Centers"
  type        = list(string)
  default     = ["ufc"]
}

variable "region" {
  description = "Região"
  type        = string
  default     = "global"
}

variable "postgres_db" {
  description = "Nome do Banco"
  default     = "wpdb"
}

variable "postgres_user" {
  description = "Usuário Postgres"
  default     = "wordpress"
}

variable "postgres_password" {
  description = "Password do Database"
  default     = "wordpress"
}

variable "meta_port" {
  description = "Metabase App"
  default     = 3000
}

job "metabase" {

  datacenters = var.datacenters
  region      = var.region
  type        = "service"

  group "metagroup" {
    count = 1

    network {
      port "admin" {
        static = var.meta_port
      }
    }

    meta {
      service = "metabase"
    }

    service {
      name     = "metabase"
      port     = "admin"
      provider = "consul"

      check {
        name     = "ready-meta-http"
        type     = "http"
        port     = "admin"
        path     = "/api/health"
        interval = "3s"
        timeout  = "2s"
      }
    }

    task "metabase" {
      driver = "docker"
      config {
        image        = "metabase/metabase:latest"
        network_mode = "host"
        ports        = ["admin"]
      }

      env {
        MB_DB_TYPE   = "mysql"
        MB_DB_DBNAME = var.postgres_db
        // MB_DB_PORT   = "5432"
        MB_DB_USER   = var.postgres_user
        MB_DB_PASS   = var.postgres_password
        // MB_DB_HOST   = "192.168.56.205"
      }

      template {
        data = <<EOH
{{- range service "mysql-server" }}
  MB_DB_HOST={{ .Address }}
  MB_DB_PORT={{ .Port }}
{{ end -}}
EOH

        destination = "local/env"
        change_mode = "restart"
        env         = true
      }

      resources {
        cpu    = 1024
        memory = 768
      }
    }
  }
}

// export NOMAD_ADDR=http://192.168.56.204:5656
