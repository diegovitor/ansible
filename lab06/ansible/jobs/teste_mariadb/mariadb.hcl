job "maria-db-server" {
  datacenters = ["ufc"]
  type        = "service"

 group "database" {
    network {
      port "db" {
        to = 3306
        static 3306
      }
    }

    service {
      name = "database"
      port = "db"
      tags = ["production", "mariadb"]
    }

    task "database" {
      driver = "docker"

      config {
        image = "mariadb"
        network_mode="host"
        ports = ["db"]
      }

      env {
        MYSQL_RANDOM_ROOT_PASSWORD = "yes"
        MYSQL_INITDB_SKIP_TZINFO   = "yes"
        MYSQL_DATABASE             = "exampledb"
        MYSQL_USER                 = "exampleuser"
        MYSQL_PASSWORD             = "examplepass"
      }

      resources {
        cpu    = 100
        memory = 512
      }
    }
  }
}
