job "wordpress" {
  datacenters = ["ufc"]

  group "database" {
    network {
      mode = "bridge"
    }

    service {
      name = "database"
      port = 3306
      tags = ["production", "mariadb"]

      connect {
        sidecar_service {}
      }
    }

    task "database" {
      driver = "docker"

      config {
        image = "mariadb"
      }

      env {
        MYSQL_RANDOM_ROOT_PASSWORD = "yes"
        MYSQL_INITDB_SKIP_TZINFO   = "yes"
        MYSQL_DATABASE             = "exampledb"
        MYSQL_USER                 = "exampleuser"
        MYSQL_PASSWORD             = "examplepass"
      }

      resources {
        cpu    = 100
        memory = 128
      }
    }
  }

  group "server" {
    network {
      mode = "bridge"

      port "http" {
        static = 8080
        to     = 80
      }
    }

    service {
      name = "wordpress"
      port = "8080"
      tags = ["production", "wordpress"]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "database"
              local_bind_port  = 3306
            }
          }
        }
      }
    }


    task "server" {
      driver = "docker"

      config {
        image = "wordpress"
      }

      env {
        WORDPRESS_DB_HOST     = "${NOMAD_UPSTREAM_ADDR_database}"
        WORDPRESS_DB_USER     = "exampleuser"
        WORDPRESS_DB_PASSWORD = "examplepass"
        WORDPRESS_DB_NAME     = "exampledb"
      }

      resources {
        cpu    = 100
        memory = 64
      }
    }
  }
} 