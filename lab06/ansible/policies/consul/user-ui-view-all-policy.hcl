/*
 ref: https://developer.hashicorp.com/consul/docs/security/acl/tokens/create/create-a-ui-token
 A política permite que os usuários que fazem login com o token visualizem todos 
 os serviços e nós no catálogo, todos os objetos  no armazenamento de chave/valor, 
 todas as intenções e todos os recursos ACL.
*/

acl = "read"
key_prefix "" {
  policy = "read"
}
node_prefix "" {
  policy = "read"
}
operator = "read"
service_prefix "" {
  policy = "read"
  intentions = "read"
}
