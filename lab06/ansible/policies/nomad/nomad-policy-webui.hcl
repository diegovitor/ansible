// https://learn.hashicorp.com/tutorials/nomad/access-control-policies
namespace "default" {
  capabilities = [
    // list-jobs - Allows listing the jobs and seeing coarse grain status.
    "list-jobs",
    // read-job - Allows inspecting a job and seeing fine grain status.
    "read-job",
    // read-logs - Allows the logs associated with a job to be viewed.
    "read-logs"
  ]
}
