# Checklist para Implantação do Nomad

## Estrutura do Cluster 
A princípio, o cluster terá 3 nós com o Nomad e o Consul em modo server e 2 nós com o Nomad e o Consul 
em modo client. 

### 3 server nodes:
  1. Configurações de rede
    - 192.168.56.101 (Lider)
    - 192.168.56.102
    - 192.168.56.103
  2. Configurações de hardware
    - 1GB para cada VM 
    - 2 vcpus por VM

### 2 client nodes:
  1. Configurações de rede
    - 192.168.56.204
    - 192.168.56.205
  2. Configurações de hardware
    - 2GB para cada VM 
    - 2 vcpus por VM

## Detalhes do Sistema Operacional
    - Debian 11 (bullseye)

## dependencias necessárias
  - apt-transport-https
  - ca-certificates
  - curl
  - software-properties-common
  - gnupg-agent
  - unzip
  - vim
  - jq
  - lsb-release
  - docker

## Script Ansible
  Estratégia para organização dos scrips de configuração.
  1. Playbook server
     - role packages
     - role docker 
     - role consul
     - role nomad
     - role encrypt
     - role policies
     - role acl token
  2. Playbook client
     - role packages
     - role docker 
     - role consul
     - role nomad
     - role encrypt
     - role acl token

## Arquivo de configuração do Nomad
  1. INFORMAÇÕS DO CLUSTER
    - `DATACENTER` - ufc
    - `REGIÃO` - dc1
    - `NOME` - server.vm01.prd
  2. Log level DEBUG
  3. Estrutura de Diretórios
    - `root`- /mnt/gfs/volumes/DADOS/...
    - `dados`
    - `logs`
  4. Plugins 
    - docker
    - cni 
    - raw_exec
  5. Configuração do token consul
  6. Reservar 1GB de RAM para o sistema operacional 

## Arquivo de configuração do Consul
  1. INFORMAÇÕS DO CLUSTER
    - `DATACENTER` - prd1.sti.ufc.br
    - `REGIÃO` - fortaleza
  2. Log level - INFO
  3. Habilitar tráfego criptografado no agente 
  4. Usar TLS em versões futuras


## CONFIGURAÇÃO DE TOKEN DO NOMAD
  - traefik
  - webui 
  - consul
  - usuário root 
  - deploy
  - usuário guest

## CONFIGURAÇÃO DE TOKEN DO CONSUL
  - traefik 
  - consul
  - usuário root 
  - deploy
  - usuário guest

## Regras de firewall 
<a definir>